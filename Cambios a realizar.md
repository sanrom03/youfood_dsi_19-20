#HITO 2

Las 5 personas que analizáis en esta fase, en el análisis top-down y bottom-up que habéis hecho, no coinciden con las que habéis entrevistado en la fase anterior de investigación. No entiendo el porqué de esta diferencia, pues cuando entreguéis el proyecto final completo, con todas las fases, no será coherente. No tiene mucho sentido, a no ser que retrocedáis de nuevo a la fase anterior y volváis a entrevistar a las personas que analizáis en esta fase. Cambiar las personas de esta fase por las que entrevistamos en la fase anterior. → he cambiado las personas entrevistadas del hito 1 (Pendiente las autorizaciones) → HECHO


El modelo Acción-Objeto-Contexto para la identificación de los requisitos no está bien estructurado. Recordad que la regla para identificarlo es que la acción se corresponde con el verbo, el objeto es el elemento sobre el que recae la acción (algo parecido al objeto directo que estudiábamos en análisis sintáctico) y el contexto es la situación, o el entorno, en la que se requiere esa acción sobre el objeto. Al hacerlo de esta forma, identificamos con más claridad los elementos (controles), y las acciones (interacciones usuario-aplicación) que luego formarán parte de nuestra interfaz de usuario. A modo de ejemplo, y usando los requerimientos que habéis identificado vosotros, sería algo similar a lo siguiente (os pongo los dos primeros requisitos que habéis identificado como "bullets"  pues no puedo meter tablas con este editor):
Acción - Buscar
Objeto - Recetas
Contexto - Menú de búsqueda de recetas
Acción - Visualizar
Objeto - Videos
Contexto - Receta seleccionada por el usuario → HECHO
En los requisitos habéis distinguido los dos consumidores como si fueran dos personas distintas. Revisad si es así. Esto enlaza con el punto anterior donde os comentaba la distinción entre personas primarias y/o secundarias.->HECHO PERO REVISADLO ALGUNO, ES EL PÁRRAFO DEL FINAL DEL DOCUMENTO
Revisad, por favor, estos puntos, y continuad con la siguiente fase del proyecto.

HITO 3
 Echo en falta la descripción de los escenarios. Recordad que los escenarios tienen que narrar situaciones reales en las que se pueden encontrar nuestros usuarios (personas), y describen cómo usan nuestras aplicación para resolver esas situaciones. Esto ayuda a centrar más nuestra aplicación con los objetivos de los usuarios. Es pura literatura, sé que se sale un poco de los procedimientos lógicos y matemáticos a los que estamos acostumbrados en la carrera que estáis haciendo, pero ayudan mucho a centrar la problemática del usuario, y pensar en él que es el objetivo de la metodología que estamos estudiando. Sería poner algunos párrafos de literatura que describen situaciones concretas., antes de describir con detalla los escenarios key path y de validación, que habéis hecho bastante bien.  A modo de ejemplo, un escenario de Leonardo, que es una de las personas que habéis detectado en la fase anterior:
"Leonardo acaba de llegar a casa, tras estar todo el día reunido en la oficina, y está muy cansado para ponerse a preparar la cena. Se sienta en el sofá, pone el telediario, y mientras coge su móvil para ver a través de la app Youfood puede buscar alguna receta que le lleve poco tiempo. Para ello usa el menú de recetas rápidas con pocos ingredientes que está habilitada en la aplicación …..". 
Como parte introductoria de estos escenarios, también se puede añadir recoger el apartado de problemas y visiones, como los ejemplos que se han visto en la teoría.
Añadid estos escenarios en vuestro diseño
